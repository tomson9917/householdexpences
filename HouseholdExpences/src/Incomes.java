import java.sql.Date;

import java.time.LocalDate;

class Incomes extends Record{

	public Incomes(String name, double ammount) {
	super(name, ammount);
	//this.date = LocalDate.now();
	}
	
	public Incomes(LocalDate date, String name, double ammount) {
		super(date, name, ammount);	
	}

	
	
	public String toString() {
		return " Incomes [date " + date + ", name=" + name + ", ammount=" + ammount + "]";
	}
}
