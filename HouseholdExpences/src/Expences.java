import java.time.LocalDate;

public class Expences extends Record{

	public Expences() {
	}
	 
	public Expences(String name, double ammount) {
		super(name, ammount);
	}
	
	public Expences(LocalDate date, String name, double ammount) {
		super(date, name, ammount);	
	}
	
	@Override
	public String toString() {
		return "Expences [date " + date + ", name=" + name + ", ammount=" + ammount + "]";
	}
	 
	 
}
