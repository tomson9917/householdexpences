import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BaseControll {

	DataReading base ;
	ArrayList<Object> list ;
	Expences expences ;
	Scanner scanner;
	
	
	public BaseControll() {
		 base = new DataReading(this);
		 list = new ArrayList<>();
		 expences = new Expences();
		 scanner = new Scanner(System.in);
	}

	void controlLoop() {
		Menu menu;

		do {
			printOptions();
			menu = getOption();
			switch (menu) {
			case ADD_INCOME:

				break;
			case ADD_EXPENCES:
				addExpences();
				break;
			case PRINT_INCOMES:

				break;
			case PRINT_EXPENCES:

				break;
			case DELETE_INCOME:

				break;
			case DELETE_EXPENCES:

				break;
			case ADD_CATEGORY:

				break;
			case PRINT_CATEGORIES:

				break;
			case EXIT:
				exit();
				break;

			default:
				base.printer("Nie ma takiej opcji, wprowadz ponownie.");
			}
		} while (menu != Menu.EXIT);

	}

	
	

	private void exit() {

	}

	public Scanner getScanner() {
		return scanner;
	}

	private Menu getOption() {
		Scanner scanner = new Scanner(System.in);
		boolean optionOk = false;
		Menu menu = null;
		while (!optionOk) {
			try {
				menu = Menu.createFromInt(scanner.nextInt());
				optionOk = true;
			} catch (NoSuchOptionException e) {
				base.printer(e.getMessage());
			} catch (InputMismatchException e) {
				base.printer("Wprowadzono wartosc, ktora nie jest liczba, podaj ponownie:");
			}
		}
		scanner.close();
		return menu;
	}

	public void addExpences() {
		base.createExpences();
		list.add(new Expences());
	}

	

	private void printOptions() {
		base.printer("Wybierz opcje: ");
		for (Menu menu : Menu.values()) {
			base.printer(menu.toString());
		}
	}

}