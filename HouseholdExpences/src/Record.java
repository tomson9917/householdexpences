import java.time.LocalDate;

public class Record {
	
	protected LocalDate date;
	protected String name;
	protected double ammount;
	
	public Record() {
	}
	
	public Record( String name, double ammount) {
		this.date = LocalDate.now();
		this.name = name;
		this.ammount = ammount;
	}
	public Record(LocalDate date, String name, double ammount) {
		this.date = date;
		this.name = name;
		this.ammount = ammount;
	}
	
	
	

	public String getName() {
		return name;
	}

	public double getAmmount() {
		return ammount;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	
	
}
