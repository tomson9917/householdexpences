

public enum Menu {
 EXIT(0, "wyjscie z programu"),
 ADD_INCOME(1, "dodanie nowego przychodu"),
 ADD_EXPENCES(2, "dodanie nowego wydatku"),
 PRINT_INCOMES(3, "wyswietl obecne przychody"),
 PRINT_EXPENCES(4, "wyswietl obecne wydatki"),	
 DELETE_INCOME(5, "Usun przychod"),
	DELETE_EXPENCES(6, "Usun wydatek"),
	ADD_CATEGORY(7, "Dodaj kategorie"),
	PRINT_CATEGORIES(8, "Wyswietl kategorie");

	
	private final int value;
	private final String description;
	
	Menu(int value, String description){
		this.value = value;
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
	@Override
	public String toString() {
		return value + " - " + description;
	}

	public static Menu createFromInt(int menu) throws NoSuchOptionException {
		try {
		return Menu.values()[menu];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new NoSuchOptionException("Brak opcji o id " + menu);
			
		}
	}
}
